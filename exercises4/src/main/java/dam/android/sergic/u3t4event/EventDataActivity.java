package dam.android.sergic.u3t4event;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import java.util.Calendar;

// TODO Ex4: Implementing all the needed interfaces.
public class EventDataActivity extends AppCompatActivity implements
                                                    View.OnClickListener, RadioGroup.OnCheckedChangeListener,
                                                    DatePickerDialog.OnDateSetListener,
                                                    TimePickerDialog.OnTimeSetListener
{
    // attributes
    private int priority;
    private TextView tvEventName;
    private EditText etPlace;
    private RadioGroup rgPriority;

    // TODO Ex4: Used variables to set date and hour.
    private String[] months;                                  // Useful because I want to show the month not in number.
    private Bundle fullDate;                                  // Bundle to save all the date values including the time.
    private DatePicker datePicker;
    private dam.android.sergic.u3t4event.TimePicker timePicker;
    private TextView tvSetDate;
    private TextView tvSetHour;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();
        priority = R.string.rbNormal;
        months = getResources().getStringArray(R.array.months);
        fullDate = new Bundle();

        // TODO Ex1.3: Setting up all values returned by Main.
        setValues();
    }

    private void setUI()
    {
        tvEventName = findViewById(R.id.tvEventName);
        Button btAccept = findViewById(R.id.btAccept);
        Button btCancel = findViewById(R.id.btCancel);
        etPlace = findViewById(R.id.etPlace);

        rgPriority = findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);

        // TODO Ex4: Bounding views and setting the listeners.
        tvSetDate = findViewById(R.id.tvSetDate);
        tvSetHour = findViewById(R.id.tvSetHour);

        // set listeners
        datePicker = new DatePicker(this);
        timePicker = new dam.android.sergic.u3t4event.TimePicker(this);
        tvSetDate.setOnClickListener(this);
        tvSetHour.setOnClickListener(this);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
    }

    // TODO Ex1.3: Restoring previous selected data.
    private void setValues()
    {
        // get data from Intent that started this activity
        Bundle inputData = getIntent().getExtras();
        if(inputData!=null)
        {
            tvEventName.setText(inputData.getString(MainActivity.EVENT_NAME));

            /* If there is more info apart from the event name. */
            if(inputData.size()>1)
            {
                etPlace.setText(inputData.getString(MainActivity.PLACE));
                setPriority(inputData.getInt(MainActivity.PRIORITY));

                fullDate.putInt(MainActivity.DAY, inputData.getInt(MainActivity.DAY));
                fullDate.putInt(MainActivity.MONTH, inputData.getInt(MainActivity.MONTH));
                fullDate.putInt(MainActivity.YEAR, inputData.getInt(MainActivity.YEAR));
                fullDate.putString(MainActivity.TIME, inputData.getString(MainActivity.TIME));
            }
            else
            {
                /* By default the date and hour is the current one. */
                // TODO Ex4: What to do if there isn't any previous date selected.
                Calendar cal = Calendar.getInstance();

                fullDate.putInt(MainActivity.DAY, cal.get(Calendar.DAY_OF_MONTH));
                fullDate.putInt(MainActivity.MONTH, cal.get(Calendar.MONTH));
                fullDate.putInt(MainActivity.YEAR, cal.get(Calendar.YEAR));

                // If the minutes are 0 then I have to change it to 00.
                String minutes = cal.get(Calendar.MINUTE)+"";
                String time = cal.get(Calendar.HOUR_OF_DAY)+":"+(minutes.length()>1? minutes : "0"+minutes);
                fullDate.putString(MainActivity.TIME,time);
            }

            tvSetDate.setText(getString(R.string.datePlaceHolder,
                      fullDate.getInt(MainActivity.DAY)+"",
                                    months[fullDate.getInt(MainActivity.MONTH)],
                                    fullDate.getInt(MainActivity.YEAR)+""));

            tvSetHour.setText(fullDate.getString(MainActivity.TIME));
        }
    }

    /* Method that with a given string priority change the selection in the view. */
    private void setPriority(int priority)
    {
        this.priority = priority;

        /* Don't need to check for normal because is the default selected. */
        if(this.priority == R.string.rbLow)
            rgPriority.check(R.id.rbLow);
        else
        {
            if(this.priority == R.string.rbHigh)
                rgPriority.check(R.id.rbHigh);
        }
    }

    @Override
    public void onClick(View v)
    {
        Intent activityResult = new Intent();
        Bundle eventData;

        switch(v.getId())
        {
            case R.id.btAccept:

                /* Creating a bundle to save all values. */
                eventData = new Bundle();

                // If the place is not defined I like to put 'Unknown'.
                String text = etPlace.getText().toString();

                // TODO Ex1.3: Sending to main all the values selected.
                /* Actually placing all the values into the bundle. */
                eventData.putString(MainActivity.EVENT_NAME, tvEventName.getText().toString());
                eventData.putString(MainActivity.PLACE, text.length()>0? text : getString(R.string.nothing));
                eventData.putInt(MainActivity.PRIORITY, priority);
                eventData.putAll(fullDate);

                /* Setting the result. */
                activityResult.putExtras(eventData);
                setResult(RESULT_OK, activityResult);
                finish();
                break;
            case R.id.btCancel:
                // TODO Ex1.1: Set the result to canceled.
                setResult(RESULT_CANCELED);
                finish();
                break;

                // TODO Ex4: TextViews with OnClick, inflating the fragments.
            case R.id.tvSetDate:
                datePicker.show(getSupportFragmentManager(), "datePicker");
                break;
            case R.id.tvSetHour:
                timePicker.show(getSupportFragmentManager(),"timePicker");
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId)
    {
        switch (checkedId)
        {
            case R.id.rbLow:
                priority = R.string.rbLow;
                break;
            case R.id.rbNormal:
                priority = R.string.rbNormal;
                break;
            case R.id.rbHigh:
                priority = R.string.rbHigh;
                break;
        }
    }

    // TODO Ex4 What happens when the user changes the date or hour.

    @Override
    public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth)
    {
        fullDate.putInt(MainActivity.DAY,dayOfMonth);
        fullDate.putInt(MainActivity.MONTH,month);
        fullDate.putInt(MainActivity.YEAR,year);

        tvSetDate.setText(getString(R.string.datePlaceHolder,
                  fullDate.getInt(MainActivity.DAY)+"",
                                months[fullDate.getInt(MainActivity.MONTH)],
                                fullDate.getInt(MainActivity.YEAR)+""));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute)
    {
        // If the minutes are 0 then I have to change it to 00.
        String minutes = minute+"";
        String time = hourOfDay+":"+(minutes.length()>1? minutes : "0"+minutes);

        fullDate.putString(MainActivity.TIME,time);
        tvSetHour.setText(fullDate.getString(MainActivity.TIME));
    }

    // TODO Ex4: Saving and restoring the information against any orientation change...

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        // Checking if there's any previous data to save.
        outState.putBundle(MainActivity.BUNDLE,fullDate);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        fullDate = savedInstanceState.getBundle(MainActivity.BUNDLE);

        tvSetDate.setText(getString(R.string.datePlaceHolder,
                fullDate.getInt(MainActivity.DAY)+"",
                months[fullDate.getInt(MainActivity.MONTH)],
                fullDate.getInt(MainActivity.YEAR)+""));

        tvSetHour.setText(fullDate.getString(MainActivity.TIME));
    }
}