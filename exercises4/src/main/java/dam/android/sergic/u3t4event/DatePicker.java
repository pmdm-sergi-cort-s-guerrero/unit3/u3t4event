package dam.android.sergic.u3t4event;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import java.util.Calendar;

public class DatePicker extends DialogFragment
{
    /* Attributes */
    private DatePickerDialog.OnDateSetListener listener;

    /* Constructor */
    public DatePicker(DatePickerDialog.OnDateSetListener listener)
    {
        this.listener = listener;
    }

    /**
     * Method that creates the date picker dialog.
     *
     * @param savedInstanceState -> A bundle.
     * @return An object DatePickerDialog.
     */
    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getContext(), listener, year, month, day);
    }
}
