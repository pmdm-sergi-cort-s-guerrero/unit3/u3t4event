package dam.android.sergic.u3t4event;

import android.app.Dialog;
import android.os.Bundle;
import android.app.TimePickerDialog;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import java.util.Calendar;

public class TimePicker extends DialogFragment
{
    private TimePickerDialog.OnTimeSetListener listener;

    public TimePicker(TimePickerDialog.OnTimeSetListener listener)
    {
        this.listener = listener;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), listener, hour, minute, true);
    }
}
