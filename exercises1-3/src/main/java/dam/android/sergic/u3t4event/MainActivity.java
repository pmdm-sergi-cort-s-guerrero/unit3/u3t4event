package dam.android.sergic.u3t4event;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    // attributes
    private EditText etEventName;
    private TextView tvCurrentData;

    private Bundle eventData;                                                                       // TODO Ex1.3: Bundle to save and manage the event data
    private String[] months;                                                                        // TODO Ex1.2: New months array.

    // constants
    public static final String EVENT_NAME = "eventName";
    public static final String PLACE = "place";
    public static final String PRIORITY = "priority";
    public static final String TIME = "time";
    public static final String DAY = "day";
    public static final String MONTH = "month";
    public static final String YEAR = "year";

    public static final int REQUEST = 0;

    public static final String BUNDLE = "bundle";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
        eventData = new Bundle();
        months = getResources().getStringArray(R.array.months);                                     // TODO Ex1.2: Loading the months.
    }

    private void setUI()
    {
        etEventName = findViewById(R.id.etEventName);
        tvCurrentData = findViewById(R.id.tvCurrentData);
    }

    /* OnClick --> To edit the event data. */
    public void editEventData(View view)
    {
        Intent intent = new Intent(this, EventDataActivity.class);

        // Setting the name.
        String eventName = etEventName.getText().toString();
        if(eventName.length()>0)
            eventData.putString(EVENT_NAME, eventName);
        else
        {
            // If there isn't any name I put 'Unknown'
            eventName = getString(R.string.nothing);
            etEventName.setText(eventName);
        }
        eventData.putString(EVENT_NAME, eventName);

        // add bundle to intent and launching the activity.
        /*
            Inside this bundle is located all the event information,
            the place, hour, date...
         */
        // TODO Ex1.3: Sending all the information.
        intent.putExtras(eventData);
        startActivityForResult(intent,REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        // TODO Ex1.1: Keep the old values unless the result is OK.
        // check if result comes from EventDataActivity and finished OK
        if(requestCode == REQUEST && resultCode == RESULT_OK)
        {
            if(data!=null)
            {
                // TODO Ex1.3: Saving all the event data.
                eventData = data.getExtras();

                // TODO Ex3: Using a placeholder to set the info.
                // Establishing the textView info.
                String date = eventData.getInt(DAY)+" "+months[eventData.getInt(MONTH)]+" "+eventData.getInt(YEAR);
                tvCurrentData.setText(getString(R.string.eventDataHolder,
                        eventData.getString(PLACE),getString(eventData.getInt(PRIORITY)),
                        date,eventData.getString(TIME)));
            }
        }
    }

    // TODO Ex1.3: Saving and restoring the information against an orientation change.

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        // Checking if there's any previous data to save.
        String data = tvCurrentData.getText().toString();
        if(!data.equals(getString(R.string.tvCurrentDataShow)))
            outState.putBundle(BUNDLE,eventData);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        Bundle savedData = savedInstanceState.getBundle(BUNDLE);
        if(savedData!=null)
        {
            // Recovering the bundle with all the info.
            eventData = savedData;

            // TODO Ex3: Using a placeholder to set the info.
            // Establishing the textView info.
            String date = eventData.getInt(DAY)+" "+months[eventData.getInt(MONTH)]+" "+eventData.getInt(YEAR);
            tvCurrentData.setText(getString(R.string.eventDataHolder,
                    eventData.getString(PLACE),getString(eventData.getInt(PRIORITY)),
                    date,eventData.getString(TIME)));
        }
    }
}