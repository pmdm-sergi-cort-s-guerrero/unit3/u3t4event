package dam.android.sergic.u3t4event;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener
{
    // attributes
    private int priority;
    private TextView tvEventName;
    private EditText etPlace;
    private RadioGroup rgPriority;
    private TimePicker tpTime;
    private DatePicker dpDate;
    private Button btAccept;
    private Button btCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();
        priority = R.string.rbNormal;

        // TODO Ex1.3: Setting up all values returned by Main.
        setValues();
    }

    private void setUI()
    {
        tvEventName = findViewById(R.id.tvEventName);
        tpTime = findViewById(R.id.tpTime);
        dpDate = findViewById(R.id.dpDate);
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);
        etPlace = findViewById(R.id.etPlace);

        rgPriority = findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);

        // set listeners
        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);

        // TODO Ex2: Depending of the device orientation I have to show or not the calendar view.
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            dpDate.setCalendarViewShown(false);
    }

    // TODO Ex1.3: Restoring previous selected data.
    private void setValues()
    {
        // get data from Intent that started this activity
        Bundle inputData = getIntent().getExtras();
        if(inputData!=null)
        {
            tvEventName.setText(inputData.getString(MainActivity.EVENT_NAME));

            /* If there is more info apart from the event name. */
            if(inputData.size()>1)
            {
                etPlace.setText(inputData.getString(MainActivity.PLACE));
                setPriority(inputData.getInt(MainActivity.PRIORITY));

                // Setting up the date picker.
                dpDate.updateDate(inputData.getInt(MainActivity.YEAR),
                        inputData.getInt(MainActivity.MONTH),
                        inputData.getInt(MainActivity.DAY));

                // setting the time picker.
                String timeToProcess = inputData.getString(MainActivity.TIME);
                if(timeToProcess!=null)
                {
                    String[] time = timeToProcess.split(":");
                    tpTime.setCurrentHour(Integer.parseInt(time[0]));
                    tpTime.setCurrentMinute(Integer.parseInt(time[1]));
                }
            }
        }
    }

    /* Method that with a given string priority change the selection in the view. */
    private void setPriority(int priority)
    {
        this.priority = priority;

        /* Don't need to check for normal because is the default selected. */
        if(this.priority == R.string.rbLow)
            rgPriority.check(R.id.rbLow);
        else
        {
            if(this.priority == R.string.rbHigh)
                rgPriority.check(R.id.rbHigh);
        }
    }

    @Override
    public void onClick(View v)
    {
        Intent activityResult = new Intent();
        Bundle eventData;

        switch(v.getId())
        {
            case R.id.btAccept:

                /* Creating a bundle to save all values. */
                eventData = new Bundle();

                // If the place is not defined I like to put 'Unknown'.
                String text = etPlace.getText().toString();

                // If the minutes are 0 then I have to change it to 00.
                String minutes = tpTime.getCurrentMinute().toString();
                String time = tpTime.getCurrentHour()+":"+(minutes.length()>1? minutes : "00");

                // TODO Ex1.3: Sending to main all the values selected.
                /* Actually placing all the values into the bundle. */
                eventData.putString(MainActivity.EVENT_NAME, tvEventName.getText().toString());
                eventData.putString(MainActivity.PLACE, text.length()>0? text : getString(R.string.nothing));
                eventData.putInt(MainActivity.PRIORITY, priority);
                eventData.putInt(MainActivity.DAY, dpDate.getDayOfMonth());
                eventData.putInt(MainActivity.MONTH,dpDate.getMonth());
                eventData.putInt(MainActivity.YEAR,dpDate.getYear());
                eventData.putString(MainActivity.TIME, time);

                /* Setting the result. */
                activityResult.putExtras(eventData);
                setResult(RESULT_OK, activityResult);

                break;
            case R.id.btCancel:
                // TODO Ex1.1: Set the result to canceled.
                setResult(RESULT_CANCELED);
                break;
        }
        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId)
    {
        switch (checkedId)
        {
            case R.id.rbLow:
                priority = R.string.rbLow;
                break;
            case R.id.rbNormal:
                priority = R.string.rbNormal;
                break;
            case R.id.rbHigh:
                priority = R.string.rbHigh;
                break;
        }
    }
}