package dam.android.sergic.u3t4event;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    // attributes
    private EditText etEventName;
    private TextView tvCurrentData;

    // constants
    public static final String EVENT_NAME = "EventName";
    public static final int REQUEST = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI()
    {
        etEventName = findViewById(R.id.etEventName);
        tvCurrentData = findViewById(R.id.tvCurrentData);
    }

    public void editEventData(View view)
    {
        Intent intent = new Intent(this, EventDataActivity.class);
        Bundle bundle = new Bundle();

        // set info data to bundle
        bundle.putString(EVENT_NAME, etEventName.getText().toString());
        // add bundle to intent
        intent.putExtras(bundle);

        startActivityForResult(intent,REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        // check if result comes from EventDataActivity and finished OK
        if(requestCode == REQUEST && resultCode == RESULT_OK)
            tvCurrentData.setText(data.getStringExtra(EventDataActivity.EVENT_DATA));
    }
}