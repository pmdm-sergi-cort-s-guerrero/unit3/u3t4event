package dam.android.sergic.u3t4event;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener
{
    // attributes
    private String priority = "Normal";
    private TextView tvEventName;
    private RadioGroup rgPriority;
    private TimePicker tpTime;
    private DatePicker dpDate;
    private Button btAccept;
    private Button btCancel;

    // constants
    public static final String EVENT_DATA = "EventData";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();

        // get data from Intent that started this activity
        Bundle inputData = getIntent().getExtras();
        tvEventName.setText(inputData.getString(MainActivity.EVENT_NAME));
    }

    private void setUI()
    {
        tvEventName = findViewById(R.id.tvEventName);
        tpTime = findViewById(R.id.tpTime);
        dpDate = findViewById(R.id.dpDate);
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);

        rgPriority = findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);

        // set listeners
        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View v)
    {
        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        switch(v.getId())
        {
            case R.id.btAccept:
                String[] month = {"January","February","March","April","May","June","July",
                                    "August","September","October","November","December"};
                eventData.putString(EVENT_DATA, "Priority: "+priority+"\n"+
                                                "Month: "+ month[dpDate.getMonth()]+"\n"+
                                                "Day: "+dpDate.getDayOfMonth()+"\n"+
                                                "Year: "+dpDate.getYear());
                break;
            case R.id.btCancel:
                eventData.putString(EVENT_DATA,"");
                break;
        }
        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId)
    {
        switch (checkedId)
        {
            case R.id.rbLow:
                priority = "Low";
                break;
            case R.id.rbNormal:
                priority = "Normal";
                break;
            case R.id.rbHigh:
                priority = "High";
                break;
        }
    }
}
